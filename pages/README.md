# Test Project

### `DEV Test`

Run `npm run dev` in the app folder for development environment.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.

### `Folder Structure`

Create `index.vue` files inside each folder in pages folder to manage each page easier, since for an admin dashboard there will be so many compoments
